Игра "Крестики-Нолики"

Данный код реализует "крестики-нолики" с возможностью играть в режиме "Игрок-Игрок" или "Игрок-Компьютер".

Код реализован посредством языка C++, использована модель программирования .NET Framework.

Программа содержит 2 Windows Forms, одна из которых (StartForm) отвечает за начальное окно, а другая (GameForm) непосредственно за игровое поле. Файлы Gamer, GameMap 
отвечают за действия игрока и за создание игрового поля соответственно. 